Nombre del proyecto: Calculadora técnicas de conteo

Descripción del proyecto: 

Codigo fuente:

Programa en el cual el usuario podra utilizar una calculadora de formulas usuales de la combinatoria, permutacion y variaciones. La cual tiene la gracia de poder elegir su tipo,orden,repeticion y cuantitavidad de las variables.
El lenguaje del codigo esta programado para ser compilado y ejecutado en c++.
**proyecto.cpp**

Informe:

El informe busca dar a conocer todos los ambitos externos e internos del proyecto de una forma mas grafica y explicita para el usuario.
**Informe-Grupo13.docx**

changelog:
El changelog busca dar a conocer los cambios efectuados en el transcurso del proyecto 
**changelog.txt** 

Calculadora:

Interfaz en la cual el usuario podra utilizar las funciones de la calculadora (Mismas funciones que codigo fuente) de una manera grafica.
El lenguaje del codigo esta dividido en distintos archivos programados para ser compilados y ejecutados con QT Creator y c++. 
- **proyectof.pro**
- **main.cpp**
- **calculadora.cpp**
- **calculadora.h**
- **calculadora.ui** 

Requisitos de Compilación o Plataforma:

Hito 1

-mingw 
-cygwin
-#Plataforma (Visual Studio, GitBash) Nosotros ocupamos estas plataformas

Hito 2

-Qt Creator 

Hito 3

-Qt Creator

¿Como instalar o compilar?

Hito 1

g++ -o Proyecto Proyecto.cpp 
./Proyecto.exe

Hito 2

Utilizar aplicacion run code de QT Creator sobre proyectof.pro (#No olvidar descargar antes main.cpp, calculadora.ui, calculadora.cpp, calculadora.h)

#¡IMPORTANTE!: En la carpeta donde se encuentran los archivos, si tiene problemas para ejecutarlo correctamente, se a dejado un archivo proyectof.rar que posee todo el código funcionando. Esto a modo de respaldo. (ya que en algunos casos nos daba problemas al reemplazar los archivos en GitLab).

Hito 3

Utilizar aplicacion run code de QT Creator sobre proyectof.pro (#No olvidar descargar antes main.cpp, calculadora.ui, calculadora.cpp, calculadora.h)

#Mismas especificaciones que Hito 2 pero con los archivos del hito 3

Datos Integrantes:

    *Lucas Vicuña ; Rol: 201866671-8 ; Par 201
    *Sebastián Jara ; Rol: 201944515-4 ; Par 201
    *Javier Acuña ; Rol: 201944537-5 ; Par 201

