#ifndef CALCULADORA_H
#define CALCULADORA_H

#include <QWidget>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class calculadora; }
QT_END_NAMESPACE

class calculadora : public QMainWindow
{
    Q_OBJECT

public:
    calculadora(QWidget *parent = nullptr);
    ~calculadora();

private slots:
    void on_var_rep_clicked();

    void on_pushButton_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_7_clicked();

private:
    Ui::calculadora *ui;
};
#endif // CALCULADORA_H
