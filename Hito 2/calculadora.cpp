#include "calculadora.h"
#include "ui_calculadora.h"

float factorial(float n){
    float fact=1;
    if (n<0){
        fact=0;
    }
    else if (n==0){
        fact=1;
    }
    else{
        for (int i = 1; i <= n; i++){
            fact = fact*i;
        }
    }
    return fact;
}
int potencia(int base, int exponente){ //calcula la potencia
    int resu=1;
    for (int i=0;i<exponente;i++){
        resu=resu*base;
    }
    return resu;
}

calculadora::calculadora(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::calculadora)
{
    ui->setupUi(this);
    setWindowTitle("Calculeitor");
    ui->resultado->setText("0");
    ui->resultado2->setText("0");
    ui->resultado3->setText("0");
    ui->resultado4->setText("0");
    ui->resultado5->setText("0");
    ui->resultado6->setText("0");
}

calculadora::~calculadora()
{
    delete ui;
}


void calculadora::on_var_rep_clicked()
{
    ui->resultado->setText(QString::number(factorial(ui->n1->value())/factorial((ui->n1->value())-(ui->n2->value()))));

}


void calculadora::on_pushButton_clicked()
{
    ui->resultado2->setText(QString::number(potencia(ui->n1_2->value(),ui->n2_2->value())));
}

void calculadora::on_pushButton_5_clicked()
{
    ui->resultado3->setText(QString::number(factorial(ui->n1_3->value())));

}

void calculadora::on_pushButton_2_clicked()
{
    ui->resultado4->setText(QString::number(factorial(ui->n1_4->value())/factorial(ui->a->value())*factorial(ui->b->value())*
                                            factorial(ui->c->value())*factorial(ui->d->value())));
}

void calculadora::on_pushButton_3_clicked()
{
    ui->resultado5->setText(QString::number(factorial(ui->n1_5->value())/((factorial((ui->n1_5->value())-(ui->n2_3->value())))*ui->n2_3->value())));
}

void calculadora::on_pushButton_4_clicked()
{
    ui->resultado6->setText(QString::number(factorial(ui->n1_6->value()+ui->n2_4->value()-1)/(((factorial((ui->n1_6->value())-1)))*factorial(ui->n2_4->value()))));
}



void calculadora::on_pushButton_7_clicked()
{
    ui->n1_2->setValue(0);
    ui->n1_3->setValue(0);
    ui->n1_4->setValue(0);
    ui->n1_5->setValue(0);
    ui->n1_6->setValue(0);
    ui->n2_2->setValue(0);
    ui->n2_3->setValue(0);
    ui->n2_4->setValue(0);
    ui->a->setValue(0);
    ui->b->setValue(0);
    ui->c->setValue(0);
    ui->d->setValue(0);
    ui->n1->setValue(0);
    ui->n2->setValue(0);
    ui->resultado->setText("0");
    ui->resultado2->setText("0");
    ui->resultado3->setText("0");
    ui->resultado4->setText("0");
    ui->resultado5->setText("0");
    ui->resultado6->setText("0");
}
