#include <iostream>
#include <math.h>

using namespace std;
/* Este programa tiene la finalidad de ayudar a los estudiantes que necesitan resolver problemas de combinatoria
para que los estudiantes puedan corroborar sus respuestas sin perder tiempo*/

//Funciones Declaraciónes
float factorial(float n);
int potencia(int base, int exponente);
int variacion_no_repeticion(int n, int r);
int variacion_repeticion(int n,int r);
int permutacion_no_repeticion(int n);
int permutacion_repeticion(int n, int a, int b, int c,int d);
int combinacion_no_repeticion(int n, int r);
int combinacion_repeticion(int n, int r);

//Funcion principal

int main(){
    char *x= new char;//Orden #opcion
    char *y= new char;//Intervencion #opcion
    char *z= new char;//Repeticion #opcion
    int n;// total de elementos
    int r;// repeticiones
    int a;// numero que se repite
    int b;// numero que se repite
    int c;// numero que se repite
    int d;// numero que se repite
    int sol;
    cout << "Bienvenid@ a la calculadora de tecnicas de conteo.\nEn este programa se encuentran distintas formulas para el desarrollo de permutacion, combinacion o variacion.\nA continuacion se le pediran las condiciones de su problema y luego los valor de n y r." << endl;
    cout<< "Utilice Y para Yes o N para No"<< endl; //Y = si , N = no
    bool flag=true;
    while (flag==true){
        cout << "¿Importa el orden?"<< endl;
        cin >> x;
        if(*x == 'Y'){
            while(flag==true){
                cout << "¿Intervienen todos los elementos?"<< endl;
                cin >> y; 
                if(*y == 'Y'){
                    while(flag==true){
                        cout << "¿Se repiten los elementos?"<< endl; 
                        cin >> z;
                        if(*z == 'Y'){ // permutacion con repeticion
                            cout << "Es una permutacion con repeticion" << endl;
                            cout<< "Ingrese 5 elementos, si son menos ingrese 0 en los que no necesite"<<endl;
                            cout << "Recuerde que r = n" << endl;
                            cout << "       n! " << endl;
                            cout<< "P = ----------" << endl;
                            cout << "   a!*b!*c!*d!" << endl;
                            cout << "Ingrese n: ";
                            cin >> n;
                            cout << "Ingrese a: ";
                            cin >> a;
                            cout << "Ingrese b: ";
                            cin >> b;
                            cout << "Ingrese c: ";
                            cin >> c;
                            cout << "Ingrese d: ";
                            cin >> d;
                            sol= permutacion_repeticion(n,a,b,c,d);
                            cout << "El resultado es: " << sol << endl;
                            flag=false;
                            break;
                        }

                        else if(*z == 'N'){ //permutación no repetición
                            cout << "Es una permutacion sin repeticion" << endl;
                            cout << "Recuerde que r = n" << endl;
                            cout << "P = n! " << endl;
                            cout << "Ingrese n: ";
                            cin >> n;
                            sol = permutacion_no_repeticion(n);
                            cout << "El resultado es: " << sol << endl;
                            flag=false;
                            break;
                        }

                        else{
                            cout << "Ingrese un valor valido" << endl;
                        }
                    }
                }

                else if(*y == 'N'){
                    while (flag=true){
                        cout << "¿Se repiten los elementos?" << endl;
                        cin >> z;
                        if(*z == 'Y'){// variaciones con repetición
                            cout << "Es una variacion con repeticion" << endl;
                            cout << "Recuerde que r < n o r > n" << endl;
                            cout << "V = n^r"<< endl;
                            cout << "Ingrese n: ";
                            cin >> n;
                            cout << "Ingrese r: ";
                            cin >> r;
                            sol = variacion_repeticion(n, r);
                            cout << "El resultado es: " << sol << endl;
                            flag=false;
                            break;
                        }

                        else if(*z == 'N'){// variaciones no repetición
                            cout << "Es una variacion sin repeticion" << endl;
                            cout << "Recuerde que r < n"<< endl;
                            cout<< "       n!"<< endl;
                            cout<< "V=  -------"<< endl;
                            cout<< "    (n - r)!" << endl;
                            cout << "Ingrese n: ";
                            cin >> n;
                            cout << "Ingrese r: ";
                            cin >> r;
                            sol = variacion_no_repeticion(n, r);
                            cout << "El resultado es: " << sol << endl;
                            flag=false;
                            break;
                        }

                        else{
                            cout << "Ingrese un valor valido" << endl;
                        }
                    }
                }
                else{
                    cout << "Ingrese un valor valido" << endl;
                }
            }
        }
        else if(*x == 'N'){
            while (flag==true){
                cout << "¿Se repiten los elementos?"<< endl;
                cin >> z; 
                if(*z == 'Y'){//combinaciones con repetición
                    cout << "Es una Combinacion con repeticion" << endl;
                    cout << "Recuerde que r <= n" << endl;
                    cout << "   (n + r - 1)!"<<endl;
                    cout << "C= ------------"<<endl;
                    cout<< "     r!*(n -1)!" << endl;
                    cout << "Ingrese n: ";
                    cin >> n;
                    cout << "Ingrese r: ";
                    cin >> r;
                    sol = combinacion_repeticion(n, r);
                    cout << "El resultado es: " << sol << endl;
                    flag=false;
                }
                else if(*z == 'N'){ // combinaciones no repetición
                    cout << "Es una Combinacion sin repeticion" << endl;
                    cout << "Recuerde que r <= n" << endl;
                    cout << "        n!"<< endl;
                    cout << "C=  ---------"<< endl;
                    cout << "   (n - r)!*r!"<< endl;
                    cout << "Ingrese n: ";
                    cin >> n;
                    cout << "Ingrese r: ";
                    cin >> r;
                    sol = combinacion_no_repeticion(n, r);
                    cout << "El resultado es: " << sol << endl;
                    flag=false;
                }
                else{
                    cout << "Ingrese un valor valido" << endl;
                }
            }
        }
        else{
            cout << "Ingrese un valor valido" << endl;
        }
    }
    cout << "Gracias por usar la calculadora tecnica de conteo" << endl;
    cout << "Patrocinado por: Javier Acuna - Lucas Vicuna - Sebastian Jara" << endl;
    delete x;
    delete y;
    delete z;
    return 0; //Fin del programa
}

//Funciones Desarrollo

//Funcion que calcula el factorial de un numero
float factorial(float n){
    float fact=1;
    if (n<0){
        fact=0;
    }
    else if (n==0){
        fact=1;
    }
    else{
        for (int i = 1; i <= n; i++){
            fact = fact*i;
        }
    }
    return fact;
}

int potencia(int base, int exponente){ //calcula la potencia
    int resu=1;
    for (int i=0;i<exponente;i++){
        resu=resu*base;
    }
    return resu;
}

int variacion_no_repeticion(int n, int r){ //Importa orden
    int resta=n-r;
    int variacion=factorial(n)/factorial(resta);
    return variacion;
}

int variacion_repeticion(int n,int r){ //Importa orden
    int variacion = potencia(n,r);
    return variacion;
}

int permutacion_no_repeticion(int n){  //Importa orden
    int permu=factorial(n);
    return permu;
}

int permutacion_repeticion(int n, int a, int b, int c,int d){  //Importa orden
    int permu = factorial(n) / (factorial(a)*factorial(b)*factorial(c)*factorial(d));
    return permu;
}

int combinacion_no_repeticion(int n, int r){  //No importa orden
    int rest= n-r;
    int comb= factorial(n) / (factorial(rest)*factorial(r));
    return comb;
}


int combinacion_repeticion(int n, int r){  //No importa orden
    int t = n + r - 1;
    int rest= n - 1;
    int comb = factorial(t) / (factorial(r)*factorial(rest)) ;
    return comb;
}
