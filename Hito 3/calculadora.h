#ifndef CALCULADORA_H
#define CALCULADORA_H

#include <QWidget>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class calculadora; }
QT_END_NAMESPACE

class calculadora : public QMainWindow
{
    Q_OBJECT

public:
    calculadora(QWidget *parent = nullptr);
    ~calculadora();

private slots:
    void on_var_Nrep_clicked();

    void on_var_rep_clicked();

    void on_perm_Nrep_clicked();

    void on_perm_rep_clicked();

    void on_comb_Nrep_clicked();

    void on_comb_rep_clicked();

    void on_clear_clicked();


private:
    Ui::calculadora *ui;
};
#endif // CALCULADORA_H
